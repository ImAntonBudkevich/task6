const loc = document.querySelector("#location");
const icon = document.querySelector("#icon");
const temp = document.querySelector("#temper");
const descr = document.querySelector("#descr");
const btn = document.querySelector("#shownhide");
const wthr = document.querySelector(".weather");
const cls = document.querySelector("#close");
const spin = document.querySelector("#spinner");

class Forecast {
  constructor(temp, descr, locat, icon) {
    this.temp = temp;
    this.descr = descr;
    this.locat = locat;
    this.icon = icon;
  }
}
function starter() {
  showSpin();
  if ("geolocation" in navigator) {
    navigator.geolocation.getCurrentPosition(
      (pos) => {
        const lon = pos.coords.longitude;
        const lat = pos.coords.latitude;
        let link = `https://api.openweathermap.org/data/2.5/weather?lat=${lat}&lon=${lon}&appid=5beb13d6e0bab7eb4cdd443bf8383e3a`;
        getWeather(link);
      },
      (err) => {
        alert(err.message);
      }
    );
  } else {
    alert("Browser don't suppose Geolocation");
  }
}

function getWeather(url) {
  fetch(url)
    .then((resp) => {
      return resp.json();
    })
    .then((data) => {
      const Oneday = new Forecast(
        data.main.temp - 273,
        data.weather[0].description,
        `${data.name}, ${data.sys.country}`,
        data.weather[0].icon
      );
      displayWeather(Oneday);
      hideSpin();
    })
    .catch(function () {});
}

function displayWeather(main) {
  loc.innerHTML = `Сейчас в ${main.locat}`;
  icon.innerHTML = `<img src=http://openweathermap.org/img/w/${main.icon}.png>`;
  temp.innerHTML = `${Math.floor(main.temp)} &deg; С`;
  descr.innerHTML = `${main.descr}`;
}

btn.addEventListener("click", () => {
  if (wthr.classList.contains("hiden")) {
    wthr.classList.add("showed");
    starter();
    wthr.classList.remove("hiden");
  } else {
    wthr.classList.add("hiden");
    wthr.classList.remove("showed");
  }
});

cls.addEventListener("click", () => {
  wthr.classList.add("hiden");
  wthr.classList.remove("showed");
});

function showSpin() {
  spin.classList.add("showed");
  spin.classList.remove("hiden");
}
function hideSpin() {
  spin.classList.remove("showed");
  spin.classList.add("hiden");
}
